#!/bin/bash
#  _____ _                           ______        ____        ____        __ 
# |_   _| |__   ___ _ __ ___   ___  / ___\ \      / /\ \      / /\ \      / / 
#   | | | '_ \ / _ \ '_ ` _ \ / _ \ \___ \\ \ /\ / /  \ \ /\ / /  \ \ /\ / /  
#   | | | | | |  __/ | | | | |  __/  ___) |\ V  V /    \ V  V /    \ V  V /   
#   |_| |_| |_|\___|_| |_| |_|\___| |____/  \_/\_/      \_/\_/      \_/\_/    
#                                                                             
#  
# by Stephan Raabe (2023) 
# ----------------------------------------------------- 

# ----------------------------------------------------- 
# Select random wallpaper and create color scheme
# ----------------------------------------------------- 
wal -q -i ~/wallpaper/

# ----------------------------------------------------- 
# Load current pywal color scheme
# ----------------------------------------------------- 
source "$HOME/.cache/wal/colors.sh"

# ----------------------------------------------------- 
# Copy current wallpaper
# ----------------------------------------------------- 
cp $wallpaper $HOME/.current_wallpaper

# ----------------------------------------------------- 
# get wallpaper iamge name
# ----------------------------------------------------- 
newwall=$(echo $wallpaper | sed "s|$HOME/wallpaper/||g")

# ----------------------------------------------------- 
# Set the new wallpaper
# ----------------------------------------------------- 
swww img $wallpaper \
    --transition-type="simple" \
    --transition-duration=1 \
    --transition-pos "$( hyprctl cursorpos )"


# rofi
RFILE="$HOME/.config/rofi/themes/colors.rasi"
cat > $RFILE <<- EOF
    /* colors */    
    * {    
    background:     ${background}90;
    background-alt: ${color2}90;
    background-tra: ${background}00;
    foreground:     ${foreground}FF;
    selected:       ${color2}FF;
    active:         ${color6}FF;
    urgent:         ${color6}FF;
    }
EOF

# ----------------------------------------------------- 
# Reload mako
# ----------------------------------------------------- 
$HOME/.config/mako/update-theme.sh

# ----------------------------------------------------- 
# Restart waybar
# -----------------------------------------------------
sleep 0.2
killall -SIGUSR2 waybar

# ----------------------------------------------------- 
# Send notification
# ----------------------------------------------------- 
notify-send "Theme and Wallpaper updated" "With image $newwall"

echo "DONE!"