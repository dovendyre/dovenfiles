#!/bin/bash

#        -lf/nf/cf color
#            Defines the foreground color for low, normal and critical notifications respectively.
# 
#        -lb/nb/cb color
#            Defines the background color for low, normal and critical notifications respectively.
# 
#        -lfr/nfr/cfr color
#            Defines the frame color for low, normal and critical notifications respectively.

[ -f "$HOME/.cache/wal/colors.sh" ] && . "$HOME/.cache/wal/colors.sh"
DUFILE="$HOME/.config/dunst/dunstrc"

pidof dunst && killall dunst

sed -i -e "s/background =.*/background = \"$color7\"/g" $DUFILE
sed -i -e "s/foreground =.*/foreground = \"$color1\"/g" $DUFILE
sed -i -e "s/frame_color =.*/frame_color = \"$color7\"/g" $DUFILE
dunst > /dev/null 2>&1 &